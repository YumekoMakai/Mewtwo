# Starts up all the services using screen all in one go.
# Currently only supports Unix based systems.

import sys
import os
import time

isProduction = '--prod' in sys.argv

os.chdir('startup/unix')

if not isProduction:
    os.system('screen -dmS MongoDB ./run_mongo.sh')

os.system('screen -dmS OTP ./run_otp_server.sh')
os.system('screen -dmS UberDOG ./run_server.sh')

# Wait until the UberDOG server starts.
time.sleep(5)

os.system('cd ../.. && screen -dmS Districts python3 -m DistrictStarter')

base = 'screen -dmS Stunnel'

if not os.getenv('IS_DOCKER', False):
    base += ' sudo'

os.system(f'cd ../../../stunnel/bin && {base} ./start_tstunnel.sh')

if isProduction:
    os.system('screen -dmS Endpoints ./run_endpoint_manager.sh')

    os.system('cd ../../../discord-status-bot && python3 -m Starter')
