#!/bin/sh
cd ../../config

export LD_PRELOAD=$LD_PRELOAD:`jemalloc-config --libdir`/libjemalloc.so.`jemalloc-config --revision`
export MALLOC_CONF=confirm_conf:true,background_thread:true

../../OtpGo/otpgo -l debug otp.yml >otpgo.log 2>&1

# valgrind --leak-check=full --show-leak-kinds=all --log-file="/tmp/valgrind.txt" ../../OtpGo/otpgo -l debug otp.yml
