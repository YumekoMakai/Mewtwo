@echo off
title Server - UberDOG
cd ../..

set /P PYTHON_PATH=<PYTHON_PATH

:main
%PYTHON_PATH% -m game.toontown.uberdog.Start config/Config.prc
pause
goto :main
