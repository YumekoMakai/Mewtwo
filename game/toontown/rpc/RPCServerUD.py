from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.stdpy import threading2

from direct.distributed.PyDatagram import PyDatagram

from game.otp.distributed import OtpDoGlobals
from game.otp.otpbase import OTPLocalizer
from game.otp.ai import AIMsgTypes

from game.toontown.ai.DatabaseObject import DatabaseObject

from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from jsonrpc import JSONRPCResponseManager, dispatcher
from threading import Thread

from panda3d.direct import DCPacker

class RPCServerUD:
    notify = directNotify.newCategory('RPCServerUD')

    def __init__(self, air):
        self.air = air

        # Start up the RPC service thread.
        Thread(target = self.startup).start()

    def packArgs(self, field, value) -> PyDatagram:
        packer = DCPacker()
        packer.beginPack(field)

        field.packArgs(packer, value)
        packer.endPack()

        data = PyDatagram()
        data.appendData(packer.getBytes())

        return data

    @Request.application
    def application(self, request):
        # Dispatcher is dictionary {<method_name>: callable}.
        dispatcher['echo'] = lambda s: s
        dispatcher['add'] = lambda a, b: a + b
        dispatcher['action'] = self.handleAction

        response = JSONRPCResponseManager.handle(request.data, dispatcher)
        return Response(response.json, mimetype = 'application/json')

    def handleAction(self, secretKey, action, arguments):
        if secretKey != config.GetString('api-token'):
            return 'Nice try.'

        if action == 'systemMessage':
            message = arguments[0]
            self.sendSystemMessage(message)
            return 'Broadcasted system message to shard.'
        elif action == 'kickPlayer':
            if len(arguments) == 2:
                avId = int(arguments[0])
                reason = arguments[1]
                self.air.extAgent.sendKick(avId, reason)
                return 'Kicked player from server.'
        elif action == 'approveName':
            db = DatabaseObject(self.air, int(arguments[0]))
            db.dclass = self.air.dclassesByName['DistributedToonUD']
            db.setFields({
                'WishNameState': self.packArgs(db.dclass.getFieldByName('WishNameState'), ('APPROVED',))
            })
            return 'Approved name.'
        elif action == 'rejectName':
            db = DatabaseObject(self.air, int(arguments[0]))
            db.dclass = self.air.dclassesByName['DistributedToonUD']
            db.setFields({
                'WishNameState': self.packArgs(db.dclass.getFieldByName('WishNameState'), ('REJECTED',))
            })
            return 'Rejected name.'
        elif action == 'banPlayer':
            return 'TODO: Implement on new OTP server.'

            avatarId = int(arguments[0])

            def handleRetrieve(dclass, fields):
                if dclass != self.air.dclassesByName['DistributedToonUD']:
                    return

                accountId = fields['setDISLid'][0]
                playToken = fields['setAccountName'][0]

                self.air.extAgent.sendKick(avatarId, 'N/A')
                self.air.extAgent.banAccount(playToken, 'N/A', 'N/A', True)

            # Query the avatar to get some account information.
            self.air.dbInterface.queryObject(self.air.dbId, avatarId, handleRetrieve)
            return 'Banned avatar.'
        elif action == 'warnPlayer':
            avId = int(arguments[0])
            reason = str(arguments[1])

            # Prepare the client message.
            clientMsg = PyDatagram()
            clientMsg.addUint16(AIMsgTypes.CLIENT_SYSTEMMESSAGE_AKNOWLEDGE)
            clientMsg.addString(reason)

            # Send it.
            dg = PyDatagram()
            dg.addServerHeader(self.GetPuppetConnectionChannel(avId), 0, AIMsgTypes.CLIENT_AGENT_SEND_DATAGRAM)
            dg.addBlob(clientMsg.getMessage())
            self.air.send(dg)

            return 'Warned avatar.'
        elif action == 'retrieveAccountId':
            return 'TODO: Implement on new OTP server.'

            playToken = str(arguments[0])
            accountId = self.air.extAgent.bridge.query(playToken)

            response = f'No account found associated with {playToken}!'

            if accountId:
                response = f'The accountId associated with {playToken} is {accountId}.'

            return response
        elif action == 'queryObject':
            return 'TODO: Implement on new OTP server.'

            doId = int(arguments[0])

            result = []
            unblocked = threading2.Event()

            def callback(dclass, fields):
                if dclass is not None:
                    dclass = dclass.getName()

                result.extend([dclass, fields])

                # Unblock, we are done.
                unblocked.set()

            self.air.dbInterface.queryObject(self.air.dbId, doId, callback)

            # Block until the callback is executed:
            unblocked.wait()

            return result

        return 'Unhandled action.'

    def sendSystemMessage(self, message):
        # Prepare the client message.
        clientMsg = PyDatagram()
        clientMsg.addUint16(AIMsgTypes.CLIENT_SYSTEM_MESSAGE)
        clientMsg.addString(message)

        # Send it.
        dg = PyDatagram()
        dg.addServerHeader(AIMsgTypes.CHANNEL_CLIENT_BROADCAST, 0, AIMsgTypes.CLIENT_AGENT_SEND_DATAGRAM)
        dg.addBlob(clientMsg.getMessage())
        self.air.send(dg)

    def startup(self):
        run_simple('0.0.0.0', 7969, self.application)
