from direct.fsm.FSM import FSM
from direct.distributed.ClockDelta import *
from direct.distributed.PyDatagram import PyDatagram

from game.toontown.hood import ZoneUtil
from game.toontown.toon.DistributedToonAI import DistributedToonAI
from .TransporterGlobals import *


class TransporterToonAI(DistributedToonAI, FSM):
    def __init__(self, mgr, name, dna):
        # HACK: Renaming our class name here because
        # DistributedObjectAI will search dclassesByName
        # for the non-existant TransporterToonAI dclass.
        self.__class__.__name__ = "DistributedToonAI"
        DistributedToonAI.__init__(self, mgr.air)
        FSM.__init__(self, name)

        self.mgr = mgr

        # Toon we are currently handling:
        self.handling = None

        # Friend request context
        self.friendContext = 0

        # Set our name and DNA string
        self.setName(name)
        self.setDNAString(dna)

        # Don't tell the server to "announce" our arrival.
        self.announceArrival = False

    def requestTask(self, name, *args):
        self.request(name, *args)

    def enterOff(self):
        if self.handling:
            self.ignore(self.air.getAvatarExitEvent(self.handling.doId))
            self.handling = None

        taskMgr.remove(self.taskName('arriveFinish'))
        taskMgr.remove(self.taskName('Timeout-1'))
        taskMgr.remove(self.taskName('Timeout-2'))
        taskMgr.remove(self.taskName('TeleportToZone'))
        taskMgr.remove(self.taskName('Happy'))
        taskMgr.remove(self.taskName('Finish'))
        taskMgr.remove(self.taskName('Off'))

        if self.getLocation()[1] != QuietZone:
            self.b_setAnimState("TeleportOut", 1)
            self.b_setLocation(self.parentId, QuietZone)

        # Cleanup friends list
        for i in range(len(self.friendsList[:])):
            friendPair = self.friendsList[:][i]

            # We are doing this call just in case they have switched districts.
            self.air.sendUpdateToDoId("DistributedToon", 'friendsNotify', friendPair[0], [self.doId, 1])

            # And now we send an update to the client (if they're still logged in)
            # telling it to delete their friend list entry.
            dg = PyDatagram()
            dg.addServerHeader(self.GetPuppetConnectionChannel(friendPair[0]), self.air.ourChannel, 3505) # TOONTOWNCLIENT_REMOVE_FRIEND
            dg.addUint32(self.doId)
            self.air.send(dg)

    def exitOff(self):
        pass

    def resetTimeout(self):
        taskMgr.remove(self.taskName('Timeout-1'))
        taskMgr.remove(self.taskName('Timeout-2'))

        taskMgr.doMethodLater(10, self.timeout1, self.taskName('Timeout-1'), extraArgs=[])
        taskMgr.doMethodLater(15, self.timeout2, self.taskName('Timeout-2'), extraArgs=[])

    def timeout1(self):
        if self.getCurrentOrNextState() == 'WaitForTeleport':
            self.air.sendUpdateToDoId("DistributedToon", 'setWhisperSCFrom', self.handling.doId, [self.doId, 109]) # Hello?
        else:
            self.sendUpdate('setSC', [109]) # Hello?

    def timeout2(self):
        if self.getCurrentOrNextState() == 'WaitForTeleport':
            self.air.sendUpdateToDoId("DistributedToon", 'setWhisperSCFrom', self.handling.doId, [self.doId, 207]) # I need to go.
        else:
            self.sendUpdate('setSC', [207]) # I need to go.

        if self.friendContext:
            # Cancel friend invite if we were waiting on one
            self.mgr.cancelFriendQuery(self.friendContext)
            self.friendContext = 0

        self.request('Off')

    def handleAvatarExit(self):
        print('handleAvatarExit')
        # The avatar we are handling has left the district,
        # Skedaddle.
        if self.getCurrentOrNextState() != 'Off':
            self.request('Off')

    def enterArrive(self, toon, h):
        self.handling = toon

        self.acceptOnce(self.air.getAvatarExitEvent(self.handling.doId), self.handleAvatarExit)

        zoneId = toon.getLocation()[1]
        self.b_setLocation(self.parentId, zoneId)
        canonZone = ZoneUtil.getCanonicalBranchZone(zoneId)
        x, y, z, _, p, r, = zone2PosHpr[canonZone]
        self.sendUpdate('setSmPosHpr', [x, y, z, h, p, r, globalClockDelta.getFrameNetworkTime()])

        self.b_setAnimState("TeleportIn", 1)
        self.sendUpdate('teleportGreeting', [toon.doId]) # Hi, <requested toon>.
        taskMgr.doMethodLater(1.8, self.arriveFinish, self.taskName('arriveFinish'), extraArgs=[])

    def arriveFinish(self):
        self.b_setAnimState("Happy", 1)
        # Check if we are friends with that toon already.
        alreadyFriends = False
        for i in range(len(self.friendsList)):
            friendPair = self.friendsList[i]
            if friendPair[0] == self.handling.doId:
                alreadyFriends = True
                break
        if alreadyFriends:
            self.request("RequestZone")
            return

        self.request('Friend')

    def exitArrive(self):
        pass

    def enterFriend(self):
        self.resetTimeout()
        self.mgr.makeFriendRequest(self, self.handling)

    # Friend related messages:
    def setFriendsList(self, friendsList):
        DistributedToonAI.setFriendsList(self, friendsList)

        # Check if our handling toon has been added.
        if self.getCurrentStateOrTransition() == "Friend":
            for i in range(len(self.friendsList)):
                friendPair = self.friendsList[i]
                if friendPair[0] == self.handling.doId and self.getCurrentOrNextState() == 'Friend':
                    self.request("RequestZone")
                    return
        else:
            for i in range(len(self.friendsList)):
                friendPair = self.friendsList[i]
                if friendPair[0] == self.handling.doId:
                    return

            # Our friend has removed us.  Time to skedaddle.
            if self.getCurrentOrNextState() != 'Off':
                self.request('Off')


    def handleFriendQuery(self, fromAv, context):
        if fromAv == self.handling.doId:
            # Accept invite.
            self.mgr.sendFriendResponse(1, context)

            if self.friendContext:
                # Some fool decided to make their own friend request even though
                # we sent ours!  Cancel it.
                self.mgr.cancelFriendQuery(self.friendContext)
                self.friendContext = 0

            # NOTE: FriendManager will update setFriendsList for us.
        else:
            # Someone else, decline invite.
            self.mgr.sendFriendResponse(0, context)

    def handleFriendConsidering(self, response, context):
        if response == 1:
            # Awaiting response from invitee.
            self.friendContext = context
        else:
            self.notFriends()

    def handleFriendResponse(self, response, context):
        if self.friendContext == context:
            self.friendContext = 0
            if response != 1:
                self.notFriends()

        # At this point they have accepted.
        # wait for FriendManager to update setFriendsList.

    def notFriends(self):
        self.sendUpdate('setSC', [507]) # Please be my friend!

    def exitFriend(self):
        pass

    def enterRequestZone(self):
        self.resetTimeout()
        # Tell the client that we are online.
        dg = PyDatagram()
        dg.addServerHeader(self.GetPuppetConnectionChannel(self.handling.doId), self.air.ourChannel, 3501) # TOONTOWNCLIENT_FRIEND_ONLINE
        dg.addUint32(self.doId)
        dg.addUint8(0)
        self.air.send(dg)

        self.accept('setSC', self.handleSC)

        self.sendUpdate('setSC', [1003]) # Where should we go?

    def setWhisperSCFrom(self, fromAv, msgId):
        # Handle whispers as normal SpeedChat messages.
        # Yes, you could do that in the original bots too.
        if self.getCurrentStateOrTransition() != "RequestZone":
            return

        if fromAv != self.handling.doId:
            return

        self.handleSC(self.handling, msgId)

    def handleSC(self, toon, msgId):
        if self.getCurrentStateOrTransition() != "RequestZone":
            return

        if toon != self.handling:
            return

        # Because we cannot handle "Let's find other toons"
        # at the moment (which was teleporting to Nutty River)
        # Respond with "Sorry, I can't.", because we cannot
        # teleport to another district.
        if msgId == 1009:
            self.sendUpdate('setSC', [807]) # Sorry, I can't.
            return


        requestedZone = SC2Zone.get(msgId)
        if not requestedZone:
            return

        if ZoneUtil.getCanonicalBranchZone(self.getLocation()[1]) == requestedZone:
            return

        self.request('TeleportToZone', requestedZone)

    def exitRequestZone(self):
        self.ignore('setSC')

    def enterTeleportToZone(self, requestedZone):
        self.resetTimeout()
        self.sendUpdate('setSC', [1000]) # Let's go!
        self.b_setAnimState('TeleportOut', 1)
        taskMgr.doMethodLater(4, self.requestTask, self.taskName('TeleportToZone'), extraArgs=['WaitForTeleport', requestedZone])

    def exitTeleportToZone(self):
        pass

    def enterWaitForTeleport(self, requestedZone):
        self.resetTimeout()
        self.b_setLocation(self.parentId, requestedZone)

        x, y, z, h, p, r, = zone2PosHpr[requestedZone]
        self.sendUpdate('setSmPosHpr', [x, y, z, h, p, r, globalClockDelta.getFrameNetworkTime()])

        self.b_setAnimState('TeleportIn', 1)
        taskMgr.doMethodLater(1.8, lambda: self.b_setAnimState('Happy', 1), self.taskName('Happy'), extraArgs=[])

        self.air.sendUpdateToDoId("DistributedToon", 'setWhisperSCFrom', self.handling.doId, [self.doId, 1001]) # Can you teleport to me?

        self.accept('teleportGreeting', self.handleGreeting)

    def teleportQuery(self, avId):
        if avId == self.handling.doId:
            self.resetTimeout()
        self.air.sendUpdateToDoId("DistributedToon", 'teleportResponse', avId, [self.doId, 1, self.parentId, ZoneUtil.getBranchZone(self.zoneId), self.zoneId])

    def handleGreeting(self, toon, toAvId):
        if self.getCurrentStateOrTransition() != "WaitForTeleport":
            return
        if toon == self.handling and toAvId == self.doId:
            taskMgr.doMethodLater(1.8, self.requestTask, self.taskName('Finish'), extraArgs=['Finish'])

    def exitWaitForTeleport(self):
        self.ignore('teleportGreeting')

    def enterFinish(self):
        self.resetTimeout()
        self.sendUpdate('setSC', [503]) # Any time!
        self.sendUpdate('setEmoteState', [13, 1, globalClockDelta.getFrameNetworkTime()])
        taskMgr.doMethodLater(4, self.requestTask, self.taskName('Off'), extraArgs=['Off'])

    def exitFinish(self):
        pass
