from panda3d.core import *
from direct.distributed.PyDatagram import PyDatagram
from direct.showbase.DirectObject import DirectObject
from direct.directnotify import DirectNotifyGlobal

from game.otp.distributed.OtpDoGlobals import OTP_DO_ID_FRIEND_MANAGER
from game.toontown.ai.ToontownAIMsgTypes import *
from game.toontown.hood import ZoneUtil

from .TransporterGlobals import *
from .TransporterToonAI import TransporterToonAI

import random

class TransporterManagerAI(DirectObject):
    notify = DirectNotifyGlobal.directNotify.newCategory('TransporterManagerAI')

    def __init__(self, air):
        DirectObject.__init__(self)
        self.air = air
        self.accept('setSC', self.handleSC)

        self.bots = []
        self.botById = {}
        self.createBots()

    def createBots(self):
        for name, dna in names2DNAString.items():
            botToon = TransporterToonAI(self, name, dna)
            # Generating to Quiet Zone is fine since
            # clients won't get any generates from that
            # zone.
            botToon.generateWithRequired(QuietZone)
            botToon.b_setParent(2)

            # Set its owner receive to us so we can
            # receive such fields.
            dg = PyDatagram()
            dg.addServerHeader(botToon.doId, self.air.ourChannel, STATESERVER_OBJECT_SET_OWNER_RECV)
            dg.addUint64(self.air.ourChannel)
            self.air.send(dg)

            self.registerBot(botToon)
            self.bots.append(botToon)
            self.botById[botToon.doId] = botToon

    def handleSC(self, toon, msgId):
        location = toon.getLocation()
        if not location:
            return

        if msgId != MEET_HERE_ID:
            return

        zoneId = location[1]
        canonZone = ZoneUtil.getCanonicalBranchZone(zoneId)
        if canonZone not in zone2PosHpr:
            return

        for botToon in self.bots:
            if botToon.getLocation()[1] == zoneId:
                return
            elif botToon.handling == toon:
                return

        spawnNode = NodePath("spawnNode")
        spawnNode.setPosHpr(*zone2PosHpr[canonZone])
        length = toon.getPos(spawnNode).length()

        if length > SPAWN_RANGE:
            spawnNode.removeNode()
            return

        spawnNode.headsUp(toon.getPos())
        h = spawnNode.getH()

        spawnNode.removeNode()

        triedBots = []
        while True:
            if len(triedBots) == len(self.bots):
                self.notify.debug("All bots are busy, breaking.")
                break
            botToon = random.choice([i for i in self.bots if i not in triedBots])
            if botToon.getCurrentStateOrTransition() != "Off":
                triedBots.append(botToon)
                continue

            botToon.request("Arrive", toon, h)
            break

    def registerBot(self, bot):
        dg = PyDatagram()
        dg.addServerHeader(OTP_DO_ID_FRIEND_MANAGER, self.air.ourChannel, FRIENDMANAGER_REGISTER_INTERNAL_SENDER)
        dg.addUint32(bot.doId)
        self.air.send(dg)

    def handleFriendMessage(self, msgType, dgi):
        if msgType == FRIENDMANAGER_INVITEE_FRIEND_QUERY:
            toAv = dgi.getUint32()
            fromAv = dgi.getUint32()
            context = dgi.getUint32()

            bot = self.botById.get(toAv)
            if not bot:
                self.notify.warning(f"Got FRIEND_QUERY to unknown doId: {toAv}")
                return

            bot.handleFriendQuery(fromAv, context)

        elif msgType == FRIENDMANAGER_INVITEE_FRIEND_CONSIDERING:
            toAv = dgi.getUint32()
            response = dgi.getUint8()
            context = dgi.getUint32()

            bot = self.botById.get(toAv)
            if not bot:
                self.notify.warning(f"Got FRIEND_CONSIDERING to unknown doId: {toAv}")
                return

            bot.handleFriendConsidering(response, context)
        elif msgType == FRIENDMANAGER_INVITEE_FRIEND_RESPONSE:
            toAv = dgi.getUint32()
            response = dgi.getUint8()
            context = dgi.getUint32()

            bot = self.botById.get(toAv)
            if not bot:
                self.notify.warning(f"Got FRIEND_RESPONSE to unknown doId: {toAv}")
                return

            bot.handleFriendResponse(response, context)
        else:
            self.notify.warning(f"Unhandled message type {msgType}")


    def makeFriendRequest(self, bot, toon):
        dg = PyDatagram()
        dg.addServerHeader(OTP_DO_ID_FRIEND_MANAGER, self.air.ourChannel, FRIENDMANAGER_QUERY_FRIEND)
        dg.addUint32(bot.doId)
        dg.addUint32(toon.doId)
        self.air.send(dg)

    def sendFriendResponse(self, response, context):
        dg = PyDatagram()
        dg.addServerHeader(OTP_DO_ID_FRIEND_MANAGER, self.air.ourChannel, FRIENDMANAGER_INVITEE_FRIEND_RESPONSE)
        dg.addUint8(response)
        dg.addUint32(context)
        self.air.send(dg)

    def cancelFriendQuery(self, context):
        dg = PyDatagram()
        dg.addServerHeader(OTP_DO_ID_FRIEND_MANAGER, self.air.ourChannel, FRIENDMANAGER_CANCEL_FRIEND_QUERY)
        dg.addUint32(context)
        self.air.send(dg)
