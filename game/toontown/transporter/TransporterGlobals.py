from panda3d.core import LPoint3
from game.toontown.toonbase.ToontownGlobals import *

# How close does a toon have to be near the spawn point
SPAWN_RANGE = 16

MEET_HERE_ID = 1012

# Spawning points.
zone2PosHpr = {
    ToontownCentral: (121.432, -53.641, 2.525, 172.954, 0, 0),
    DonaldsDock: (-120.683, -9.544, 5.692, 261.427, 0, 0),
    MinniesMelodyland: (-106.755, -54.641, 6.525, 282.834, 0, 0),
    DaisyGardens: (50.138, 210.970, 10.025, 133.932, 0, 0),
    TheBrrrgh: (-117.803, -129.155, 6.192, 236.636, 0, 0),
    DonaldsDreamland: (87.837,  71.037,  -17.493, 178.717, 0, 0),
    GoofySpeedway: (-43.737, -141.457, -0.706, 30.301, 0, 0),
    OutdoorZone: (-217.294, 74.271, 0.025, 240.586, 0, 0),
    GolfZone: (24.495, -131.409, 0.026, 323.114, 0, 0),
    SellbotHQ: (24.747, -230.347, -13.785, 381.452, 0, 0),
    CashbotHQ: (370.845, 100.352, -23.439, 136.738, 0, 0),
    LawbotHQ: (289.852, -261.317, -68.367, 136.335, 0, 0),
    BossbotHQ: (-5.743, -134.393, 0.185, 475.439, 0, 0),
    # Toon Land
    19000: (-123.052, -2.794, 10.026, 285.595, 0, 0)
}

# Speedchat ID to their zones.
SC2Zone = {
    1105: ToontownCentral,
    1106: DonaldsDock,
    1107: MinniesMelodyland,
    1108: DaisyGardens,
    1109: TheBrrrgh,
    1110: DonaldsDreamland,
    1111: GoofySpeedway,
    1125: OutdoorZone,
    1126: GolfZone,
    1114: SellbotHQ,
    1119: CashbotHQ,
    1122: LawbotHQ,
    1127: BossbotHQ,
    # Toon Land
    31051: 19000
}

# TODO: These are all the original bots used when they first appeared
# (I think I got them all). A few more of these has been made
# around Summer 2013 when this became more widespread, find them and include them here.
# (Found Crazy Coconut in https://youtu.be/4oS-E0at69U?t=132)
# (and Crazy Chunky in https://youtu.be/KMJ-Vvr6QBw?t=160)
names2DNAString = {
    "Crazy Chester": b't\x03\x01\x02\x01\x01\x06\x01\x06\x00\t\t\x00\t\t',
    "Crazy Cuddles": b't\x05\x04\x00\x00\x00\x02\x00\x02\x00\x03\x18\x00\x18\x18',
    "Crazy Cecil": b't\x10\x01\x00\x01\x00\x03\x00\x03\x01\x04\x06\x00\x06\x06',
    "Crazy Curly": b't\x1e\x01\x01\x01\x01\x06\x01\x06\x01\x00\x10\x00\x10\x10',
    "Crazy Clover": b't\x13\x04\x01\x00\x00\x15\x00\x15\x00\x02\t\x00\t\t',
    "Crazy Candy": b't\x0f\x04\x01\x00\x00\x08\x00\x08\x00\t\x0e\x00\x0e\x0e',
    "Crazy Clyde": b't\x10\x01\x02\x01\x00\x00\x00\x00\x00\n\x02\x00\x02\x02',
    "Crazy Carol": b't\r\x04\x02\x00\x00\t\x00\t\x00\x1b\n\x00\n\n',
    "Crazy Coconut": b't\x05\x04\x01\x00\x01\x08\x01\x08\x00\x04\x03\x00\x03\x03',
    "Crazy Chunky": b't\n\x00\x02\x01\x00\x04\x00\x04\x01\x14\x10\x00\t\r'
}
