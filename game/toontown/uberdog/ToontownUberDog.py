"""
The Toontown Uber Distributed Object Globals server.
"""

from pandac.PandaModules import *
import time
if __debug__:
    from direct.showbase.PythonUtil import *

from direct.directnotify.DirectNotifyGlobal import directNotify

from game.otp.distributed import OtpDoGlobals
from game.otp.ai.AIMsgTypes import *
from game.otp.ai import TimeManagerAI
from game.otp.uberdog.UberDog import UberDog

from game.otp.friends.AvatarFriendsManagerUD import AvatarFriendsManagerUD
from game.toontown.uberdog.DistributedDeliveryManagerUD import DistributedDeliveryManagerUD
from game.toontown.uberdog.DistributedMailManagerUD import DistributedMailManagerUD
from game.toontown.parties import ToontownTimeManager
from game.toontown.rpc.RATManagerUD import RATManagerUD
from game.toontown.rpc.AwardManagerUD import AwardManagerUD
from game.toontown.uberdog import TTSpeedchatRelayUD
from game.toontown.uberdog import DistributedInGameNewsMgrUD
from game.toontown.uberdog import DistributedCpuInfoMgrUD

from game.otp.uberdog.RejectCode import RejectCode

from game.toontown.rpc.RPCServerUD import RPCServerUD
from game.toontown.uberdog.ServerBase import ServerBase

class ToontownUberDog(UberDog, ServerBase):
    notify = directNotify.newCategory("UberDog")

    def __init__(
            self, mdip, mdport, esip, esport, dcFilenames,
            serverId, minChannel, maxChannel):
        assert self.notify.debugStateCall(self)
        # TODO: The UD needs to know server time, but perhaps this isn't
        # the place to do this? -SG-SLWP
        self.toontownTimeManager = ToontownTimeManager.ToontownTimeManager()
        self.toontownTimeManager.updateLoginTimes(time.time(), time.time(), globalClock.getRealTime())

        def isManagerFor(name):
            return len(uber.objectNames) == 0 or name in uber.objectNames
        self.isFriendsManager = False # latest from Ian this should not run anymore
        #self.isFriendsManager = isManagerFor('friends')
        self.isSpeedchatRelay = isManagerFor('speedchatRelay')
        self.isGiftingManager = isManagerFor('gifting')
        self.isMailManager = False # isManagerFor('mail')
        self.isPartyManager = isManagerFor('party')
        self.isRATManager = False # isManagerFor('RAT')
        self.isAwardManager = isManagerFor('award')
        self.isCodeRedemptionManager = isManagerFor('coderedemption')
        self.isInGameNewsMgr = isManagerFor('ingamenews')
        self.isCpuInfoMgr = isManagerFor('cpuinfo')
        self.isRandomSourceManager = False # isManagerFor('randomsource')

        ServerBase.__init__(self)

        UberDog.__init__(
            self, mdip, mdport, esip, esport, dcFilenames,
            serverId, minChannel, maxChannel)

    def createObjects(self):
        UberDog.createObjects(self)
        # Ask for the ObjectServer so we can check the dc hash value
        context=self.allocateContext()
        self.queryObjectAll(self.serverId, context)

        if self.isFriendsManager:
            self.playerFriendsManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_PLAYER_FRIENDS_MANAGER,
                "TTPlayerFriendsManager")

        if self.isSpeedchatRelay:
            self.speedchatRelay = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_SPEEDCHAT_RELAY,
                "TTSpeedchatRelay")

        if self.isGiftingManager:
            self.deliveryManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_DELIVERY_MANAGER,
                "DistributedDeliveryManager")

        if self.isMailManager:
            self.mailManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_MAIL_MANAGER,
                "DistributedMailManager")

        if self.isPartyManager:
            self.partyManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_PARTY_MANAGER,
                "DistributedPartyManager")

        if simbase.config.GetBool('want-ddsm', 1):
            self.dataStoreManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_TEMP_STORE_MANAGER,
                "DistributedDataStoreManager")

        if self.isRATManager:
            self.RATManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_RAT_MANAGER,
                "RATManager")

        if self.isAwardManager:
            self.awardManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_AWARD_MANAGER,
                "AwardManager")

        if config.GetBool('want-code-redemption', 1):
            if self.isCodeRedemptionManager:
                self.codeRedemptionManager = self.generateGlobalObject(
                    OtpDoGlobals.OTP_DO_ID_TOONTOWN_CODE_REDEMPTION_MANAGER,
                    "TTCodeRedemptionMgr")

        if self.isInGameNewsMgr:
            self.inGameNewsMgr = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_IN_GAME_NEWS_MANAGER,
                "DistributedInGameNewsMgr")

        if self.isCpuInfoMgr:
            self.cpuInfoMgr = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_CPU_INFO_MANAGER,
                "DistributedCpuInfoMgr")

        if self.isRandomSourceManager:
            self.randomSourceManager = self.generateGlobalObject(
                OtpDoGlobals.OTP_DO_ID_TOONTOWN_NON_REPEATABLE_RANDOM_SOURCE,
                "NonRepeatableRandomSource")

        if self.isProdServer():
            self.rpcServer = RPCServerUD(self)

    def getDatabaseIdForClassName(self, className):
        return DatabaseIdFromClassName.get(
            className, DefaultDatabaseChannelId)

    if __debug__:
        def status(self):
            if self.isGiftingManager:
                print("deliveryManager is", self.deliveryManager)
            if self.isFriendsManager:
                print("playerFriendsManager is ",self.playerFriendsManager)

