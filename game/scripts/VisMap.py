import sys
sys.path.append('../..')

from game.toontown.toonbase import ToontownGlobals
from game.toontown.hood import ZoneUtil
from panda3d.toontown import DNAStorage, loadDNAFileAI
from game import genDNAFileName, extractGroupName
import json

validVisZones = [
    ToontownGlobals.SillyStreet,
    ToontownGlobals.LoopyLane,
    ToontownGlobals.PunchlinePlace,
    ToontownGlobals.BarnacleBoulevard,
    ToontownGlobals.SeaweedStreet,
    ToontownGlobals.LighthouseLane,
    ToontownGlobals.ElmStreet,
    ToontownGlobals.MapleStreet,
    ToontownGlobals.OakStreet,
    ToontownGlobals.AltoAvenue,
    ToontownGlobals.BaritoneBoulevard,
    ToontownGlobals.TenorTerrace,
    ToontownGlobals.WalrusWay,
    ToontownGlobals.SleetStreet,
    ToontownGlobals.PolarPlace,
    ToontownGlobals.LullabyLane,
    ToontownGlobals.PajamaPlace,
    ToontownGlobals.SellbotHQ,
    ToontownGlobals.SellbotFactoryExt,
    ToontownGlobals.CashbotHQ,
    ToontownGlobals.LawbotHQ
]

zoneVisDict = {}

for zoneId in validVisZones:
    branchZoneId = ZoneUtil.getBranchZone(zoneId)
    hoodId = ZoneUtil.getHoodId(branchZoneId)

    dnaStore = DNAStorage()
    dnaFileName = genDNAFileName(branchZoneId)
    loadDNAFileAI(dnaStore, f'../resources/{dnaFileName}')

    for i in range(dnaStore.getNumDNAVisGroupsAI()):
        visGroup = dnaStore.getDNAVisGroupAI(i)
        groupFullName = visGroup.getName()
        visZoneId = int(extractGroupName(groupFullName))
        visZoneId = ZoneUtil.getTrueZoneId(visZoneId, zoneId)
        visibles = []

        for i in range(visGroup.getNumVisibles()):
            visibles.append(int(visGroup.getVisibleName(i)))

        visibles.append(ZoneUtil.getBranchZone(visZoneId))
        zoneVisDict[visZoneId] = visibles

with open('../../otp/vismap.json', 'w') as f:
    json.dump(zoneVisDict, f)
