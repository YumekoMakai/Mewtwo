# TODO: Rebuild Panda3D and update to 24.04
FROM ubuntu:22.04

# Extend the system environments
ENV LD_LIBRARY_PATH /lib:/usr/lib:/usr/local/lib
ENV DEBIAN_FRONTEND=noninteractive

# Install all the required packages
RUN apt update
RUN apt install -y build-essential pkg-config fakeroot python3-dev libpng-dev libjpeg-dev libtiff-dev zlib1g-dev libssl-dev libx11-dev libgl1-mesa-dev libxrandr-dev libxxf86dga-dev libxcursor-dev bison flex libfreetype6-dev libvorbis-dev libeigen3-dev libopenal-dev libode-dev libbullet-dev nvidia-cg-toolkit libgtk-3-dev libassimp-dev libopenexr-dev
RUN apt install -y wget software-properties-common curl git swig libmysqlclient-dev screen autoconf
RUN add-apt-repository ppa:deadsnakes/ppa && apt update
RUN apt install -y python3.10
RUN rm /usr/bin/python3 && ln -s /usr/bin/python3.10 /usr/bin/python3

# Install pip
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3

# Grab and install Panda3D.
RUN wget -P /panda3d https://download.alt.sunrise.games/binaries/linux/panda3d1.11_1.11.0_amd64_otp.deb
RUN cd /panda3d \
    && dpkg -i panda3d1.11_1.11.0_amd64_otp.deb

# Grab and install Go.
RUN add-apt-repository ppa:longsleep/golang-backports
RUN apt-get update && apt-get install -y golang-go

# Grab and install jemalloc
RUN git clone https://github.com/jemalloc/jemalloc /jemalloc
RUN cd /jemalloc && ./autogen.sh && make && make install

# Copy all files into Docker image
COPY . /server
WORKDIR /server

RUN git clone https://gitlab.com/SunriseMMOs/stunnel /stunnel
# RUN git clone https://gitlab.com/SunriseMMOs/discord-status-bot /discord-status-bot

# Clone and build our OTP server.
RUN git clone https://github.com/LittleToonCat/OtpGo /OtpGo
RUN cd /OtpGo \
    && go build .

# Install the Discord bot's dependencies
# RUN cd /discord-status-bot && python3 -m pip install -r requirements.txt

# Install Mewtwo's pip dependencies
RUN python3 -m pip install -r requirements.txt

# Run the server
CMD chmod +x ./entrypoint.sh && ./entrypoint.sh
