-- Enable membership on dev mode
WANT_MEMBERSHIP = false

-- Enable SpeedChat+ on dev mode
WANT_SPEEDCHAT_PLUS = false

-- Enable open chat (True Friends) on dev mode
WANT_OPEN_CHAT = false

-- Production settings:
PRODUCTION_ENABLED = false

API_TOKEN = ""
PLAY_TOKEN_KEY = ""
WANT_TOKEN_EXPIRATIONS = false

-- Friend settings:
SECRET_FRIEND_STORAGE = "../otp/databases/friendCodes.json"

-- DNA validation:
DNA_FIRST_CHAR = "t"
DNA_LENGTH = 15

DNA_MAX_HEAD_INDEX = 34
DNA_MAX_TORSO_INDEX = 9
DNA_MAX_LEG_INDEX = 3

DNA_MAX_SHIRTS = 151
DNA_MAX_CLOTHING_COLORS = 31
DNA_MAX_SLEEVES = 138

DNA_MAX_MALE_BOTTOMS = 58
DNA_MAX_FEMALE_BOTTOMS = 63

DNA_MAX_COLOR = 27

-- Discord settings
NAME_APPROVAL_WEBHOOK_URL = ""
